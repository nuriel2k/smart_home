#include "mu_test.h"
#include "agent.hpp" //Agent header
#include "smoke_detector_x01.hpp" //SmokeDetectorX01 header
#include "subscribers.hpp" //Subscribers header
#include "event_id.hpp" //EventId header
#include "server.hpp" //Server header
#include "server_tcp.hpp" //ServerTcp header
#include "message_decoder.hpp" //MessageDecoder header
#include "json.hpp" //json header
#include <string> //String header
#include <iostream> //std::cout, endl


using namespace advcpp;


BEGIN_TEST(subscriber_simple_test_one_insert)
{
    using std::vector;
    vector<EventId> eventIds;
    EventId eventId("BUILDING_01__FLOOR_03__ROOM_05", "SMOKE_DETECTED");
    eventIds.push_back(eventId);
    vector<Agent*> vectorAgents;
    vectorAgents.push_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", eventIds, "SMOKE_DETECTED"));
    

    Subscribers subscribers(vectorAgents);

    auto sizeOuter = subscribers.GetContainerSize();
    auto sizeInner = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_05");
    ASSERT_EQUAL(sizeOuter, 1);
    ASSERT_EQUAL(sizeInner, 1);
}
END_TEST


BEGIN_TEST(subscriber_test_four_different_locations)
{
    using std::vector;
    const size_t AMOUT = 4;

    vector<EventId> eventIds;
    vector<Agent*> agents;
    string location = "BUILDING_01__FLOOR_03__ROOM_0";

    for(size_t i = 0 ; i < AMOUT ; ++i)
    {   
        location.append(i , '1');
        agents.push_back(new SmokeDetectorX01("id-test", "type-test", location, eventIds, "SMOKE_DETECTED"));
    }

    Subscribers subscribers(agents);

    auto sizeOuter = subscribers.GetContainerSize();
    auto sizeInner1 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_0");
    auto sizeInner2 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_01");
    auto sizeInner3 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_0111");
    auto sizeInner4 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_0111111");

    ASSERT_EQUAL(sizeOuter, 4);
    ASSERT_EQUAL(sizeInner1, 1);
    ASSERT_EQUAL(sizeInner2, 1);
    ASSERT_EQUAL(sizeInner3, 1);
    ASSERT_EQUAL(sizeInner4, 1);
}
END_TEST


BEGIN_TEST(subscriber_test_same_location_four_events)
{
    using std::vector;
    const size_t AMOUT = 4;

    vector<EventId> eventIds;
    string event = "SMOKE_DETECTED";
    vector<Agent*> agents;

    for(size_t i = 0 ; i < AMOUT ; ++i)
    {   
        event.append(i, 1);
        agents.push_back(new SmokeDetectorX01("id-test", "type-test", "same_location", eventIds, event));
    }

    Subscribers subscribers(agents);

    auto sizeOuter = subscribers.GetContainerSize();
    auto sizeInner = subscribers.GetContainerSizeInner("same_location");
    ASSERT_EQUAL(sizeOuter, 1);
    ASSERT_EQUAL(sizeInner, 4);
}
END_TEST


BEGIN_TEST(single_location_and_event_add_one_interest_event)
{
    using std::vector;
    vector<EventId> eventIds;
    vector<Agent*> agents;
    agents.push_back(new SmokeDetectorX01("id-test", "type-test", "same_location", eventIds, "SMOKE_DETECTED"));

    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests;
    subscriberIterests.push_back(EventId("same_location", "SMOKE_DETECTED"));
    auto subscriber = new SmokeDetectorX01("id-test", "type-test", "other_location", subscriberIterests, "SMOKE_DETECTED");

    auto result1 = subscribers.GetContainerSizeInner("same_location");
    auto numSubscribed = subscribers.AddSubscriber(subscriber);
    auto result2 = subscribers.GetNumSubscribers("same_location", "SMOKE_DETECTED");

    ASSERT_EQUAL(result1, 1);
    ASSERT_EQUAL(result2, 1);
    ASSERT_EQUAL(numSubscribed, 1);
}
END_TEST


BEGIN_TEST(single_location_and_event_do_not_add_one_wrong_event)
{
    using std::vector;

    vector<EventId> eventIds;    
    vector<Agent*> agents;
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", eventIds, "SMOKE_DETECTED"));
    Subscribers subscribers(agents);

    string subscriber = "test-subscriber";
    vector<EventId> subscriberIterests;
    subscriberIterests.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "LAUD_NOISE"));
    auto agent = new SmokeDetectorX01("id-test", "type-test", "other_location", subscriberIterests, "SMOKE_DETECTED");

    auto numSubscribed = subscribers.AddSubscriber(agent);
    auto result2 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_05", "SMOKE_DETECTED");

    ASSERT_EQUAL(numSubscribed, 0);
    ASSERT_EQUAL(result2, 0);
}
END_TEST


BEGIN_TEST(multiple_locations_and_events_add_one_interest_event)
{
    using std::vector;
    vector<EventId> events;
    vector<Agent*> agents;
    
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", events, "NOISE_DETECTED"));
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_06", events, "SMELL_DETECTED"));
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_02__FLOOR_03__ROOM_05", events, "LIGHT_DETECTED"));
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_02__FLOOR_04__ROOM_05", events, "SMOKE_DETECTED"));

    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests;
    subscriberIterests.push_back(EventId("BUILDING_02__FLOOR_03__ROOM_05", "LIGHT_DETECTED"));
    auto agent = new SmokeDetectorX01("id-test", "type-test", "BUILDING_02__FLOOR_04__ROOM_05", subscriberIterests, "SMOKE_DETECTED");

    auto numSubscribed = subscribers.AddSubscriber(agent);
    auto result = subscribers.GetContainerSizeInner("BUILDING_02__FLOOR_03__ROOM_05");

    ASSERT_EQUAL(numSubscribed, 1);
    ASSERT_EQUAL(result, 1);
}
END_TEST


BEGIN_TEST(multiple_locations_and_events_two_subscribers_two_interests_each)
{
    using std::vector;
    const size_t EVENTS = 4;
    vector<EventId> events;
    vector<Agent*> agents;

    string location = "BUILDING_01__FLOOR_03__ROOM_05";
    const char* arr1[EVENTS] = {"1", "2", "3", "4"};
    const char* arr2[EVENTS] = {"NOISE_DETECTED", "SMELL_DETECTED", "LIGHT_DETECTED", "SMOKE_DETECTED"};

    for(size_t i = 0 ; i < EVENTS ; ++i)
    {
        location.replace(10, 1, arr1[i]);
        agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", location, events, arr2[i]));
    }

    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests1;
    subscriberIterests1.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED"));
    subscriberIterests1.push_back(EventId("BUILDING_02__FLOOR_03__ROOM_05", "SMELL_DETECTED"));
    auto agent1 = new SmokeDetectorX01("id-test", "type-test", "BUILDING_02__FLOOR_04__ROOM_05", subscriberIterests1, "SMOKE_DETECTED");


    vector<EventId> subscriberIterests2;
    subscriberIterests2.push_back(EventId("BUILDING_03__FLOOR_03__ROOM_05", "LIGHT_DETECTED"));
    subscriberIterests2.push_back(EventId("BUILDING_04__FLOOR_03__ROOM_05", "SMOKE_DETECTED"));
    auto agent2 = new SmokeDetectorX01("id-test", "type-test", "BUILDING_02__FLOOR_04__ROOM_05", subscriberIterests2, "SMOKE_DETECTED");


    auto numSubscribed1 = subscribers.AddSubscriber(agent1);
    auto numSubscribed2 = subscribers.AddSubscriber(agent2);

    for(size_t i = 0 ; i < EVENTS ; ++i)
    {
        location.replace(10, 1, arr1[i]);
        auto result = subscribers.GetContainerSizeInner(location);
        ASSERT_EQUAL(result, 1);
    }

    auto result1 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED");
    auto result2 = subscribers.GetNumSubscribers("BUILDING_02__FLOOR_03__ROOM_05", "SMELL_DETECTED");
    auto result3 = subscribers.GetNumSubscribers("BUILDING_03__FLOOR_03__ROOM_05", "LIGHT_DETECTED");
    auto result4 = subscribers.GetNumSubscribers("BUILDING_04__FLOOR_03__ROOM_05", "SMOKE_DETECTED");

    ASSERT_EQUAL(result1, 1);
    ASSERT_EQUAL(result2, 1);
    ASSERT_EQUAL(result3, 1);
    ASSERT_EQUAL(result4, 1);

    ASSERT_EQUAL(numSubscribed1, 2);
    ASSERT_EQUAL(numSubscribed2, 2);
}
END_TEST


BEGIN_TEST(double_event_interest_from_same_subscriber)
{
    using std::vector;
    vector<EventId> eventIds;
    vector<Agent*> agents;
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", eventIds, "NOISE_DETECTED"));
    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests;
    subscriberIterests.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED"));
    subscriberIterests.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED"));

    auto agent = new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_01", subscriberIterests, "SMELL_DETECTED");
    auto numSubscribed = subscribers.AddSubscriber(agent);
    
    auto result1 = subscribers.GetContainerSize();
    auto result2 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_05");
    auto result3 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED");

    
    ASSERT_EQUAL(result1, 1);
    ASSERT_EQUAL(result2, 1);
    ASSERT_EQUAL(result3, 1);
    ASSERT_EQUAL(numSubscribed, 1);
}
END_TEST


BEGIN_TEST(double_event_interest_from_different_subscribers)
{
    using std::vector;
    vector<Agent*> agents;
    vector<EventId> eventIds;
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", eventIds, "NOISE_DETECTED"));
    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests1;
    subscriberIterests1.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED"));
    auto agent1 = new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_01", subscriberIterests1, "SMELL_DETECTED");

    vector<EventId> subscriberIterests2;
    subscriberIterests2.push_back(EventId("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED"));
    auto agent2 = new SmokeDetectorX01("id-test2", "type-test2", "BUILDING_01__FLOOR_03__ROOM_01", subscriberIterests2, "SMELL_DETECTED");

    auto numSubscribed1 = subscribers.AddSubscriber(agent1);
    auto numSubscribed2 = subscribers.AddSubscriber(agent2);

    auto result1 = subscribers.GetContainerSize();
    auto result2 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_05");
    auto result3 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED");
    
    ASSERT_EQUAL(result1, 1);
    ASSERT_EQUAL(result2, 1);
    ASSERT_EQUAL(result3, 2);
    ASSERT_EQUAL(numSubscribed1, 1);
    ASSERT_EQUAL(numSubscribed2, 1);
}
END_TEST


BEGIN_TEST(subscribe_to_all_locations_with_event_type)
{
    using std::vector;
    vector<EventId> eventIds;
    vector<Agent*> agents;
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_05", eventIds, "NOISE_DETECTED"));
    agents.emplace_back(new SmokeDetectorX01("id-test", "type-test", "BUILDING_01__FLOOR_03__ROOM_03", eventIds, "NOISE_DETECTED"));
    Subscribers subscribers(agents);

    vector<EventId> subscriberIterests1;
    subscriberIterests1.push_back(EventId("", "NOISE_DETECTED"));
    auto agent2 = new SmokeDetectorX01("id-test2", "type-test2", "BUILDING_01__FLOOR_03__ROOM_01", subscriberIterests1, "SMELL_DETECTED");
    auto numSubscribed1 = subscribers.AddSubscriber(agent2);

    auto result1 = subscribers.GetContainerSize();
    auto result2 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_05");
    auto result3 = subscribers.GetContainerSizeInner("BUILDING_01__FLOOR_03__ROOM_03");
    auto result4 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_05", "NOISE_DETECTED");
    auto result5 = subscribers.GetNumSubscribers("BUILDING_01__FLOOR_03__ROOM_03", "NOISE_DETECTED");

    ASSERT_EQUAL(result1, 2);
    ASSERT_EQUAL(result2, 1);
    ASSERT_EQUAL(result3, 1);
    ASSERT_EQUAL(result4, 1);
    ASSERT_EQUAL(result5, 1);
    ASSERT_EQUAL(numSubscribed1, 2);
}
END_TEST


BEGIN_TEST(server_simple_test_send_and_receive_event)
{
    const size_t EVENT_CAPACITY = 1;
    const size_t THREAD_CAPACITY = 1;
    const size_t CLIENT_CAPACITY = 2;

    Server server(EVENT_CAPACITY, CLIENT_CAPACITY, THREAD_CAPACITY);
    auto agentContainer = server.GetAgents();
    Agent* Agent1 = agentContainer[0];
    auto event = Agent1->ParseEvent();

    Agent1->PublishEvent(event, &server);

    ASSERT_PASS();
}
END_TEST


BEGIN_TEST(smart_home_simple_test)
{
    Server server(10, 10, 10);

    ASSERT_PASS();
}
END_TEST


BEGIN_SUITE(不耻下问)
    
// TEST(subscriber_simple_test_one_insert)
// TEST(subscriber_test_four_different_locations)
// TEST(subscriber_test_same_location_four_events)
// TEST(single_location_and_event_add_one_interest_event)
// TEST(single_location_and_event_do_not_add_one_wrong_event)
// TEST(multiple_locations_and_events_add_one_interest_event)
// TEST(multiple_locations_and_events_two_subscribers_two_interests_each)
// TEST(double_event_interest_from_same_subscriber)
// TEST(double_event_interest_from_different_subscribers)
// TEST(subscribe_to_all_locations_with_event_type)
// TEST(smart_home_simple_test)
TEST(server_simple_test_send_and_receive_event)
END_SUITE
