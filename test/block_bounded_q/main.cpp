#include "mu_test.h" //mu_tests header
#include <vector> //std::vector
#include <memory> //std::shared_ptr
#include <algorithm> //std::is_sorted
// #include <utility>
#include "thread.hpp" //Thread header
#include "producer.hpp" //Producer header
#include "consumer.hpp" //Consumer header
#include "vector_helpers.hpp" //VectorHelpers header
#include "block_bounded_q.hpp" //BlockBoundedQ header
#include "void_method_void_adapt.hpp" //ResultMethodArgAdapt header
#include "group_thread.hpp" //GroupThread header
// #include "consumer_v2.hpp"
// #include "producer_v2.hpp"


using namespace advcpp;
using namespace test_widgets;
using Adapter = VoidMethodVoidAdapt<Producer&, void(Producer::*)(void)>;
using ProducerAdapter = VoidMethodVoidAdapt<Producer&, void(Producer::*)(void)>;
using ConsumerAdapter = VoidMethodVoidAdapt<Consumer&, void(Consumer::*)(void)>;



BEGIN_TEST(test_queue_get_capacity)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N);
    ASSERT_EQUAL(queue.GetCapacity(), N);

END_TEST


BEGIN_TEST(test_queue_is_empty)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N); 
    ASSERT_EQUAL(queue.IsEmpty(), true);

END_TEST


BEGIN_TEST(test_queue_is_full)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N);
    Producer producer(queue, N); 
    std::shared_ptr<Adapter> adapter = std::make_shared<Adapter>(producer, &Producer::FillContainer);

    {
        Thread thread(adapter, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.IsFull(), true);

END_TEST


BEGIN_TEST(test_queue_get_size)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N);
    Producer producer(queue, N);
    std::shared_ptr<Adapter> adapter = std::make_shared<Adapter>(producer, &Producer::FillContainer);
    
    {
        Thread thread(adapter, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.GetSize(), N);

END_TEST

    
BEGIN_TEST(one_producer_one_consumer_10_values)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N);
    Producer prod(queue, N);
    std::shared_ptr<ProducerAdapter> adapterProdc = std::make_shared<ProducerAdapter>(prod, &Producer::FillContainer);

    {
        Thread thread1(adapterProdc, Thread::JOIN);
    }

    Consumer consumer(queue, N); 
    std::shared_ptr<ConsumerAdapter> adapterCons = std::make_shared<ConsumerAdapter>(consumer, &Consumer::ConsumeContainer);
    {
        Thread thread2(adapterCons, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.GetSize(), 0);
    bool result = std::is_sorted(consumer.GetInnerContainer().begin(), consumer.GetInnerContainer().end());
    ASSERT_EQUAL(result, true);
    
END_TEST


BEGIN_TEST(one_producer_one_consumer_1000_values)
    const size_t N = 1000;

    BlockBoundedQ<size_t> queue(N);
    Producer prod(queue, N);
    std::shared_ptr<ProducerAdapter> adapterProdc = std::make_shared<ProducerAdapter>(prod, &Producer::FillContainer);
    
    {
        Thread thread1(adapterProdc, Thread::JOIN);
    }

    Consumer consumer(queue, N); 
    std::shared_ptr<ConsumerAdapter> adapterCons = std::make_shared<ConsumerAdapter>(consumer, &Consumer::ConsumeContainer);
    
    {
        Thread thread2(adapterCons, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.GetSize(), 0);
    bool result = std::is_sorted(consumer.GetInnerContainer().begin(), consumer.GetInnerContainer().end());
    ASSERT_EQUAL(result, true);
    
END_TEST


BEGIN_TEST(two_producers_one_consumer_1000_values)
    const size_t N = 10;

    BlockBoundedQ<size_t> queue(N);
    Producer prod(queue, N);
    std::shared_ptr<ProducerAdapter> adapterProdc = std::make_shared<ProducerAdapter>(prod, &Producer::FillContainer);

    
    Thread thread1(adapterProdc, Thread::JOIN);
    Thread thread2(adapterProdc, Thread::JOIN);
    
    
    Consumer consumer(queue, N * 2);
    std::shared_ptr<ConsumerAdapter> adapterCons = std::make_shared<ConsumerAdapter>(consumer, &Consumer::ConsumeContainer);
    
    {
        Thread thread3(adapterCons, Thread::JOIN);
    }

    ASSERT_EQUAL(consumer.Size(), N * 2); //check all values consumed

    ASSERT_EQUAL(queue.GetSize(), NONE); //check all values consumed
    
    std::vector<size_t>::iterator resultItr = std::unique(consumer.GetInnerContainer().begin(), consumer.GetInnerContainer().end());
    ASSERT_EQUAL(*resultItr, *consumer.GetInnerContainer().end()); //check if consumed any duplicats
    
END_TEST


BEGIN_TEST(one_producer_two_consumers_1000_values)
    const size_t PRODUCER_AMOUNT = 20;
    const size_t CONSUMER_AMOUNT = 10;
    const size_t QUEUE_CAPACITY = 5;

    BlockBoundedQ<size_t> queue(QUEUE_CAPACITY);

    Producer producer(queue, PRODUCER_AMOUNT);
    auto adapterProdc = std::make_shared<ProducerAdapter>(producer, &Producer::FillContainer);
    Thread thread1(adapterProdc, Thread::JOIN);

    Consumer consumer1(queue, CONSUMER_AMOUNT); 
    Consumer consumer2(queue, CONSUMER_AMOUNT); 

    auto adapterCons1 = std::make_shared<ConsumerAdapter>(consumer1, &Consumer::ConsumeContainer);
    auto adapterCons2 = std::make_shared<ConsumerAdapter>(consumer2, &Consumer::ConsumeContainer);
    
    {
        Thread thread2(adapterCons1, Thread::JOIN);
        Thread thread3(adapterCons2, Thread::JOIN);
    }

    ASSERT_EQUAL(consumer1.Size(), CONSUMER_AMOUNT);
    ASSERT_EQUAL(consumer2.Size(), CONSUMER_AMOUNT);
    ASSERT_PASS(); 
    
END_TEST


BEGIN_TEST(two_producer_two_consumers_1000_values)
    const size_t PRODUCER_AMOUNT = 500;
    const size_t CONSUMER_AMOUNT = 500;
    const size_t QUEUE_CAPACITY = 5;

    BlockBoundedQ<size_t> queue(QUEUE_CAPACITY);
    Producer producer(queue, PRODUCER_AMOUNT);
    Consumer consumer(queue, CONSUMER_AMOUNT); 

    auto adapterProdc = std::make_shared<ProducerAdapter>(producer, &Producer::FillContainer);
    Thread thread1(adapterProdc, Thread::JOIN);
    Thread thread2(adapterProdc, Thread::JOIN);

    auto adapterCons = std::make_shared<ConsumerAdapter>(consumer, &Consumer::ConsumeContainer);   
    {
        Thread thread3(adapterCons, Thread::JOIN);
        Thread thread4(adapterCons, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.GetSize(), 0);

    auto resultItr = std::unique(consumer.GetInnerContainer().begin(), consumer.GetInnerContainer().end());
    ASSERT_EQUAL(*resultItr, *consumer.GetInnerContainer().end()); //checking duplicates
    
END_TEST


BEGIN_TEST(fifty_producer_50_consumers_1000_values)
    const size_t PRODUCER_AMOUNT = 20;
    const size_t CONSUMER_AMOUNT = 20;
    const size_t QUEUE_CAPACITY = 20;

    BlockBoundedQ<size_t> queue(QUEUE_CAPACITY);
    Producer producer(queue, PRODUCER_AMOUNT);
    Consumer consumer(queue, CONSUMER_AMOUNT); 

    auto adapterProdc = std::make_shared<ProducerAdapter>(producer, &Producer::FillContainer);

    GroupThread gpProducer(adapterProdc, 50, Thread::JOIN);

    auto adapterCons = std::make_shared<ConsumerAdapter>(consumer, &Consumer::ConsumeContainer);   
    {
        GroupThread gpConsumer(adapterCons, 50, Thread::JOIN);
    }

    ASSERT_EQUAL(queue.GetSize(), 0);

    auto resultItr = std::unique(consumer.GetInnerContainer().begin(), consumer.GetInnerContainer().end());
    ASSERT_EQUAL(*resultItr, *consumer.GetInnerContainer().end()); //checking duplicates
    
END_TEST


// BEGIN_TEST(test_3_producers_one_consumer)
//     BlockBoundedQ<size_t> queue(500);
//     const size_t N = 1000;

//     std::shared_ptr<ProducerV2<size_t, 0UL, N>> p1 = std::make_shared<ProducerV2<size_t, 0UL, N>>(queue);
//     std::shared_ptr<ProducerV2<size_t, N, 2*N>> p2 = std::make_shared<ProducerV2<size_t, N, 2*N>>(queue);
//     std::shared_ptr<ProducerV2<size_t, 2*N, 3*N>> p3 = std::make_shared<ProducerV2<size_t, 2*N, 3*N>>(queue);


//     auto categories = std::vector<std::pair<size_t, size_t>>{
//         std::pair<size_t, size_t>{0, N},
//         std::pair<size_t, size_t>{N, 2*N},
//         std::pair<size_t, size_t>{2*N, 3*N}
//     };

//     std::shared_ptr<ConsumerV2<size_t>> c1 = std::make_shared<ConsumerV2<size_t>>(queue, categories, 4*N);


//     {
//         Thread th1(p1, Thread::JOIN);
//         Thread th2(p2, Thread::JOIN);
//         Thread th3(p3, Thread::JOIN);
//         Thread th4(c1, Thread::JOIN);
//     }

//     ASSERT_THAT(queue.IsEmpty());
//     ASSERT_THAT(c1->wasOk());

// END_TEST



BEGIN_SUITE(不耻下问)

    // TEST(test_queue_get_capacity)
    // TEST(test_queue_is_empty)
    // TEST(test_queue_is_full)
    // TEST(test_queue_get_size)
    // TEST(one_producer_one_consumer_10_values)
    // TEST(one_producer_one_consumer_1000_values)
    // TEST(two_producers_one_consumer_1000_values)
    // TEST(one_producer_two_consumers_1000_values)
    // TEST(two_producer_two_consumers_1000_values)
    // TEST(test_3_producers_one_consumer) TODO - make neccecary adaptions for this to work
    TEST(fifty_producer_50_consumers_1000_values)


END_SUITE