#include "location.hpp" //Location header


namespace advcpp
{

bool Location::operator==(const Location& a_other) const
{
    if(a_other.m_floor == m_floor && a_other.m_room == m_room)
    {
        return true;
    }
    return false;
}


Location::Location(int a_floor, int a_room)
: m_floor(a_floor)
, m_room(a_room) {}


int Location::GetFloor() const
{
    return m_floor;
}


int Location::GetRoom() const
{
    return m_room;
}
    
}
