# smart_home
A system of individual components to automate the control in a single building function.
Using agents to represent hardware components, a user can customize his own agent class to fit his hardware needs.
By using a Thread Pool and TCP data communication, Agents can publish events which other agents (that are registered to them) will receive.
Registration to event is done by choosing the type and location of the event to register to.

# example
The below example demonstrate the use of event distribution system.
An event is published by an agent representing a smoke detector, and received at an agent representing a sprinkler, which had been registed to the smoke event.
 
CONSOLE #1
```C++
#include "mu_test.h"
#include "hardware_client.hpp" //HardwareClient header
#include "json.hpp" //json
#include <string> //std::string
#include <iostream> //std::cout

using json = nlohmann::json;
using namespace advcpp;
using std::string;

BEGIN_TEST(simple_test_publish_event)

    HardwareClient sensor(1025, "127.0.0.1");
    HardwareClient sprinkler(1025, "127.0.0.1");

    string data1;
    string data2;
    sensor.ReceiveFrom(data1);
    sprinkler.ReceiveFrom(data2);

    std::cout << data1 << std::endl;
    std::cout << data2 << std::endl;

    if(data1 != "connection successfull" && data2 != "connection successfull")
    {
        ASSERT_FAIL("failed");
    }

    json sensorJson1;
    sensorJson1["size"] = 43;
    sensorJson1["op"] = 'c';
    sensorJson1["id"] = "SMOKE_DETECT_01";

    string rawMessage1 = sensorJson1.dump();
    sensor.SendTo(rawMessage1); //sensor sends agent Id

    json sprinklerJson1;
    sensorJson1["size"] = 40;
    sensorJson1["op"] = 'c';
    sensorJson1["id"] = "SPRINKLER_01"; //sprinkler sends agent Id

    json sensorJson2;
    sensorJson2["size"] = 140;
    sensorJson2["op"] = 'e';
    sensorJson2["ts"] = "saturday__07/08//2021__13:20";
    sensorJson2["type"] = "SMOKE_DETECTED";
    sensorJson2["pl"] = "test-payload";
    sensorJson2["loc"] = "BUILDING_01__FLOOR_02__ROOM_03";

    string rawMessage2 = sensorJson2.dump();
    sensor.SendTo(rawMessage2); //sensor sends an event

    ASSERT_PASS();

END_TEST

BEGIN_SUITE(不耻下问)

TEST(test_publish_event)

END_SUITE
```

CONSOLE #2
```C++
#include "mu_test.h"
#include "server_socket.hpp" //ServerSocket header
#include "socket.hpp" //socket header
#include "selector.hpp" //Selector header
using namespace advcpp;

BEGIN_TEST(simple_test_listen_accept_receive_message)
{
    ServerSocket server;
    server.Listen(2);
    auto clientSock =  server.Accept();
    
    Socket clientSocket(clientSock);
    clientSocket.Send("connection successfull");

    string receivedData;
    clientSocket.Receive(receivedData);

    std::cout << receivedData << std::endl;

    ASSERT_PASS();
}
END_TEST


BEGIN_SUITE(不耻下问)

TEST(simple_test_listen_accept_receive_message)

END_SUITE

```
