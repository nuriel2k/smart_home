#ifndef VOID_FUNC_ARG_ADAPTER_HPP_
#define VOID_FUNC_ARG_ADAPTER_HPP_
#include "callable.hpp"


namespace advcpp
{

template<typename FuncType, typename ArgType>
class VoidFuncArgAdapter : public Callable 
{
public:
    VoidFuncArgAdapter(FuncType& a_func, ArgType a_arg) : m_func(a_func), m_arg(a_arg){}
    virtual ~VoidFuncArgAdapter() = default;
    
private:
    void Execute() override {m_func(m_arg);}

private:
    FuncType& m_func;
    ArgType m_arg;
};

}

#endif //VOID_FUNC_ARG_ADAPTER_HPP_

