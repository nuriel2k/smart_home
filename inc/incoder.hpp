#ifndef INCODER_HPP_
#define INCODER_HPP_
#include "event.hpp" //EventHeader
#include <string> //std::string

namespace advcpp
{

using std::string;

class Incoder
{
public:
    Incoder() = default;
    string Incode(Event a_event) const;

private:

};


}



#endif // INCODER_HPP_