#ifndef LOCATION_HPP_
#define LOCATION_HPP_
#include <string> //std::string


namespace advcpp
{

using std::string;
class Location
{
public:
    // ctor, dtor
    Location(int a_floor, int a_room);
    Location(const Location& a_other) = default;
    ~Location() = default;

    // class methods
    bool operator==(const Location& a_other) const;
    int GetFloor() const;
    int GetRoom() const;
    
private:
    int m_floor;
    int m_room;
};

}


#endif // LOCATION_HPP_
